﻿namespace WillisTowersWatson.InterviewProblem.Console
{
   using System;
   using System.IO;
   using WillisTowersWatson.InterviewProblem.Core;

   internal class Program
   {
      private static void Main(string[] args)
      {
         if(args.Length == 2)
         {
            var filenameIn = args[0];
            var filenameOut = args[1];

            Console.WriteLine("Begining test.");

            using(var streamReader = new StreamReader(filenameIn))
            {
               using(var streamWriter = new StreamWriter(filenameOut))
               {
                  // TODO: Set up logger and pass to reader and builder.
                  var productsReaderWriter = new CsvProductsReaderWriter(streamReader, streamWriter);
                  var productsService = new ProductsService(productsReaderWriter);
                  productsService.Build();
                  productsService.Write();
               }
            }

            Console.WriteLine("Finished test.");
         }
         else
         {
            Console.WriteLine("Error: Expecting two arguments specifying an absolute input and output file paths.");
         }

         Console.WriteLine("Press any key...");
         Console.ReadKey();
      }
   }
}
