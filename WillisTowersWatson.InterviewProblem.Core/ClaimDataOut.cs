﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   using System.Collections.Generic;

   public class ClaimDataOut : IClaimDataOut
   {
      internal ClaimDataOut(int earliestOriginYear, int numberOfDevelopmentYears, IEnumerable<(string Product, IEnumerable<decimal> Values)> products)
      {
         this.EarliestOriginYear = earliestOriginYear;
         this.NumberOfDevelopmentYears = numberOfDevelopmentYears;
         this.Products = products;
      }

      public int EarliestOriginYear { get; }
      public int NumberOfDevelopmentYears { get; }
      public IEnumerable<(string Product, IEnumerable<decimal> Values)> Products { get; }
   }
}