﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   using System.Collections.Generic;

   public interface IProductsService
   {
      IEnumerable<IProduct> Products { get; }

      bool Build();

      bool Write();
   }
}