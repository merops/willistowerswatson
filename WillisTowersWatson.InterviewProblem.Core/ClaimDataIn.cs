﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   public class ClaimDataIn : IClaimDataIn
   {
      internal ClaimDataIn(string product, int originYear, int developmentYear, decimal value)
      {
         this.Product = product;
         this.OriginYear = originYear;
         this.DevelopmentYear = developmentYear;
         this.Value = value;
      }

      public string Product { get; }
      public int OriginYear { get; }
      public int DevelopmentYear { get; }
      public decimal Value { get; }
   }
}