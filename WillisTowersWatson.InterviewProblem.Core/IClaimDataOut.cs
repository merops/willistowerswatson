﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   using System.Collections.Generic;

   public interface IClaimDataOut
   {
      int EarliestOriginYear { get; }
      int NumberOfDevelopmentYears { get; }
      IEnumerable<(string Product, IEnumerable<decimal> Values)> Products { get; }
   }
}