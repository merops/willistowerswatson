﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   using System.Collections.Generic;
   using System.Linq;

   public class Product : IProduct
   {
      internal Product(string name, IEnumerable<IClaim> originYearClaims)
      {
         this.Name = name;
         this.OriginYearClaims = originYearClaims.ToList();
      }

      public string Name { get; }
      public IReadOnlyList<IClaim> OriginYearClaims { get; }
   }
}