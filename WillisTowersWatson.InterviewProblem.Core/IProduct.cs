﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   using System.Collections.Generic;

   public interface IProduct
   {
      string Name { get; }
      IReadOnlyList<IClaim> OriginYearClaims { get; }
   }
}