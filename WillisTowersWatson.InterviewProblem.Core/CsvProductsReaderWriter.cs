﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   using System;
   using System.Collections.Generic;
   using System.IO;
   using CsvHelper;
   using Microsoft.Extensions.Logging;

   public class CsvProductsReaderWriter : ProductsReaderWriter
   {
      private readonly ILogger _logger;
      private readonly StreamReader _streamReader;
      private readonly StreamWriter _streamWriter;

      public CsvProductsReaderWriter(StreamReader streamReader, StreamWriter streamWriter, ILogger logger = null)
      {
         _streamReader = streamReader;
         _streamWriter = streamWriter;
         _logger = logger;
      }

      protected internal override bool TryWrite(ClaimDataOut claimData)
      {
         try
         {
            using(var csv = new CsvWriter(_streamWriter))
            {
               // TODO: Refactor below to use WriteAsync().

               csv.WriteField(claimData.EarliestOriginYear);
               csv.WriteField(claimData.NumberOfDevelopmentYears);
               foreach(var product in claimData.Products)
               {
                  csv.NextRecord();
                  csv.WriteField(product.Product);
                  foreach(var value in product.Values)
                  {
                     csv.WriteField(Math.Round(value)); // decimal to int.
                  }
               }
            }
         }
         catch(Exception e)
         {
            Logger.LogException(_logger, e);
            return false;
         }

         return true;
      }

      protected override bool Read(out IEnumerable<IClaimDataIn> claimData)
      {
         claimData = null;
         var csvClaimData = new List<ClaimDataIn>();

         try
         {
            using(var csv = new CsvReader(_streamReader))
            {
               // TODO: Refactor below to use ReadAsync().

               csv.Configuration.HasHeaderRecord = true;
               csv.Read(); // Header
               while(csv.Read())
               {
                  csvClaimData.Add(
                     new ClaimDataIn(
                        csv.GetField(0),
                        int.Parse(csv.GetField(1)),
                        int.Parse(csv.GetField(2)),
                        decimal.Parse(csv.GetField(3))));
               }
            }
         }
         catch(Exception e)
         {
            Logger.LogException(_logger, e);
            return false;
         }

         claimData = csvClaimData;
         return true;
      }
   }
}