﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   public interface IClaimDataIn
   {
      string Product { get; }
      int OriginYear { get; }
      int DevelopmentYear { get; }
      decimal Value { get; }
   }
}