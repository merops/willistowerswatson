﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   using System.Collections.Generic;

   public abstract class ProductsReaderWriter
   {
      internal IEnumerable<IClaimDataIn> ClaimsData { get; private set; }

      protected internal abstract bool TryWrite(ClaimDataOut claimData);

      protected abstract bool Read(out IEnumerable<IClaimDataIn> claimData);

      internal bool TryRead()
      {
         if(this.Read(out var claimsData))
         {
            this.ClaimsData = claimsData;
            return true;
         }

         return false;
      }
   }
}