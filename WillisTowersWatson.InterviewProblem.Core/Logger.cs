﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   using System;
   using System.Text;
   using Microsoft.Extensions.Logging;

   internal class Logger
   {
      internal static void LogMessage(ILogger logger, string message)
      {
         logger?.LogInformation(message);
      }

      internal static void LogException(ILogger logger, Exception exception)
      {
         if(logger == null)
         {
            return;
         }

         var sb = new StringBuilder(exception.Message);

         while(exception.InnerException != null)
         {
            exception = exception.InnerException;
            sb.Append(exception.Message);
         }

         logger.LogError(sb.ToString());
      }
   }
}