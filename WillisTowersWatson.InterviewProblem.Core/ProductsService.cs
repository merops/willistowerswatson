﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using Microsoft.Extensions.Logging;

   public class ProductsService : IProductsService
   {
      private readonly ILogger _logger;
      private readonly ProductsReaderWriter _readerWriter;
      private readonly List<(string Product, IEnumerable<decimal> Values)> _writeValues = new List<(string Product, IEnumerable<decimal> Values)>();
      private int _maxYear;

      private int _minYear;

      public ProductsService(ProductsReaderWriter readerWriter, ILogger logger = null)
      {
         _logger = logger;
         _readerWriter = readerWriter;
      }

      public IEnumerable<IProduct> Products { get; private set; }

      public bool Build()
      {
         var products = new List<IProduct>();

         try
         {
            if(_readerWriter.TryRead())
            {
               _minYear = _readerWriter.ClaimsData.Min(x => x.OriginYear);
               _maxYear = _readerWriter.ClaimsData.Max(x => x.DevelopmentYear);

               foreach(var product in _readerWriter.ClaimsData.Select(x => x.Product).Distinct().OrderBy(x => x))
               {
                  var claims = new List<Claim>();
                  var cumulativeValues = new List<decimal>();

                  for(var originYear = _minYear; originYear <= _maxYear; originYear++)
                  {
                     decimal cumulativeValue = 0;

                     for(var developmentYear = originYear; developmentYear <= _maxYear; developmentYear++)
                     {
                        var value = _readerWriter.ClaimsData.SingleOrDefault(
                           x =>
                              x.Product == product &&
                              x.OriginYear == originYear &&
                              x.DevelopmentYear == developmentYear);

                        if(value != null)
                        {
                           claims.Add(new Claim(developmentYear, value.Value));
                           cumulativeValue += value.Value;
                        }

                        cumulativeValues.Add(cumulativeValue);
                     }
                  }

                  products.Add(new Product(product, claims));
                  _writeValues.Add((product, cumulativeValues));
               }

               this.Products = products;
               return true;
            }

            Logger.LogMessage(_logger, "Unable to read claims data.");
         }
         catch(Exception e)
         {
            Logger.LogException(_logger, e);
         }

         return false;
      }

      public bool Write() => _readerWriter.TryWrite(new ClaimDataOut(_minYear, _maxYear - _minYear + 1, _writeValues));
   }
}