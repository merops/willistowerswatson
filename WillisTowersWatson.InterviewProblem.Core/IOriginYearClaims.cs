﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   using System.Collections.Generic;

   public interface IOriginYearClaims
   {
      int OriginYear { get; }
      IReadOnlyList<IClaim> Claims { get; }
   }
}