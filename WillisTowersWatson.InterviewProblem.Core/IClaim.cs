﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   public interface IClaim
   {
      int Year { get; }
      decimal Value { get; }
   }
}