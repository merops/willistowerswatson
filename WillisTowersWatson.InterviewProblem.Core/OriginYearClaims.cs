﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   using System.Collections.Generic;
   using System.Linq;

   public class OriginYearClaims : IOriginYearClaims
   {
      internal OriginYearClaims(int originYear, IEnumerable<IClaim> claims)
      {
         this.OriginYear = originYear;
         this.Claims = claims.ToList();
      }

      public int OriginYear { get; }
      public IReadOnlyList<IClaim> Claims { get; }
   }
}