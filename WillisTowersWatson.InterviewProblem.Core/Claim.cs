﻿namespace WillisTowersWatson.InterviewProblem.Core
{
   public class Claim : IClaim
   {
      internal Claim(int year, decimal value)
      {
         this.Year = year;
         this.Value = value;
      }

      public int Year { get; }
      public decimal Value { get; }
   }
}